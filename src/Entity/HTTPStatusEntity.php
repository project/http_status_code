<?php

namespace Drupal\http_status_code\Entity;

use Drupal\Core\Config\Entity\ConfigEntityBase;

/**
 * Defines the HTTP Status entities entity.
 *
 * @ConfigEntityType(
 *   id = "http_status_entity",
 *   label = @Translation("HTTP Status entities"),
 *   handlers = {
 *     "view_builder" = "Drupal\Core\Entity\EntityViewBuilder",
 *     "list_builder" = "Drupal\http_status_code\HTTPStatusEntityListBuilder",
 *     "form" = {
 *       "add" = "Drupal\http_status_code\Form\HTTPStatusEntityForm",
 *       "edit" = "Drupal\http_status_code\Form\HTTPStatusEntityForm",
 *       "delete" = "Drupal\http_status_code\Form\HTTPStatusEntityDeleteForm"
 *     },
 *     "route_provider" = {
 *       "html" = "Drupal\http_status_code\HTTPStatusEntityHtmlRouteProvider",
 *     },
 *   },
 *   config_prefix = "http_status_entity",
 *   admin_permission = "administer http status code",
 *   entity_keys = {
 *     "id" = "id",
 *     "label" = "label",
 *     "uuid" = "uuid"
 *   },
 *   config_export = {
 *     "id",
 *     "label",
 *     "url",
 *     "status_code"
 *   },
 *   links = {
 *     "canonical" = "/admin/config/http_status_code/http_status_entity/{http_status_entity}",
 *     "add-form" = "/admin/config/http_status_code/http_status_entity/add",
 *     "edit-form" = "/admin/config/http_status_code/http_status_entity/{http_status_entity}/edit",
 *     "delete-form" = "/admin/config/http_status_code/http_status_entity/{http_status_entity}/delete",
 *     "collection" = "/admin/config/http_status_code/http_status_entity"
 *   }
 * )
 */
class HTTPStatusEntity extends ConfigEntityBase implements HTTPStatusEntityInterface {

  /**
   * The HTTP Status entities ID.
   *
   * @var string
   */
  protected $id;

  /**
   * The HTTP Status entities label.
   *
   * @var string
   */
  protected $label;

}
