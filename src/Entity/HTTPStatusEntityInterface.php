<?php

namespace Drupal\http_status_code\Entity;

use Drupal\Core\Config\Entity\ConfigEntityInterface;

/**
 * Provides an interface for defining HTTP Status entities entities.
 */
interface HTTPStatusEntityInterface extends ConfigEntityInterface {

  // Add get/set methods for your configuration properties here.
}
