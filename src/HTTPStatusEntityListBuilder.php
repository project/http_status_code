<?php

namespace Drupal\http_status_code;

use Drupal\Core\Config\Entity\ConfigEntityListBuilder;
use Drupal\Core\Entity\EntityInterface;

/**
 * Provides a listing of HTTP Status entities entities.
 */
class HTTPStatusEntityListBuilder extends ConfigEntityListBuilder {

  /**
   * {@inheritdoc}
   */
  public function buildHeader() {
    $header['label'] = $this->t('Label');
    $header['url'] = $this->t('Url');
    $header['status_code'] = $this->t('Status');
    return $header + parent::buildHeader();
  }

  /**
   * {@inheritdoc}
   */
  public function buildRow(EntityInterface $entity) {
    $row['label'] = $entity->label();
    $row['url'] = $entity->get('url');
    $row['status_code'] = $entity->get('status_code');
    return $row + parent::buildRow($entity);
  }

}
