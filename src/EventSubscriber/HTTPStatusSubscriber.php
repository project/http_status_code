<?php

namespace Drupal\http_status_code\EventSubscriber;

use Drupal\Core\Entity\EntityTypeManagerInterface;
use Symfony\Component\HttpKernel\KernelEvents;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;
use Symfony\Component\HttpKernel\Event\ResponseEvent;

/**
 * Class HTTPStatusSubscriber.
 */
class HTTPStatusSubscriber implements EventSubscriberInterface {

  /**
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected $entityTypeManager;

  /**
   * Constructor.
   *
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entityTypeManager
   */
  public function __construct(EntityTypeManagerInterface $entityTypeManager) {
    $this->entityTypeManager = $entityTypeManager;
  }

  /**
   * Code that should be triggered on event specified.
   */
  public function onRespond(ResponseEvent $event) {
    $response = $event->getResponse();
    $request_url = $_SERVER['REQUEST_URI'];
    $config = $this->entityTypeManager->getStorage('http_status_entity');
    $url_from_config = $config->loadByProperties(['url' => $request_url]);
    if ($set_status = reset($url_from_config)) {
      $response->setStatusCode($set_status->status_code);
    }
  }

  /**
   * {@inheritdoc}
   */
  public static function getSubscribedEvents() {
    $events[KernelEvents::RESPONSE][] = ['onRespond'];
    return $events;
  }

}
