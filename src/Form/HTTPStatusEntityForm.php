<?php

namespace Drupal\http_status_code\Form;

use Drupal\Core\Entity\EntityForm;
use Drupal\Core\Form\FormStateInterface;
use Symfony\Component\HttpFoundation\Response;

/**
 * Class HTTPStatusEntityForm.
 */
class HTTPStatusEntityForm extends EntityForm {

  /**
   * {@inheritdoc}
   */
  public function form(array $form, FormStateInterface $form_state) {
    $form = parent::form($form, $form_state);

    $http_status_entity = $this->entity;

    $form['label'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Label'),
      '#maxlength' => 255,
      '#default_value' => $http_status_entity->label(),
      '#description' => $this->t("Label for the HTTP Status entities."),
      '#required' => TRUE,
    ];
    $form['url'] = [
      '#type' => 'textfield',
      '#title' => $this->t('URL'),
      '#maxlength' => 255,
      '#default_value' => $http_status_entity->get('url'),
      '#description' => $this->t("URL for the HTTP Status entities. Must start with /"),
      '#required' => TRUE,
    ];

    $codes = $this->getStatusCodes();

    $form['status_code'] = [
      '#type' => 'select',
      '#default_value' => $http_status_entity->get('status_code'),
      '#description' => $this->t('Select status code'),
      '#required' => TRUE,
      '#options' => $codes,
    ];

    $form['id'] = [
      '#type' => 'machine_name',
      '#default_value' => $http_status_entity->id(),
      '#machine_name' => [
        'exists' => '\Drupal\http_status_code\Entity\HTTPStatusEntity::load',
      ],
      '#disabled' => !$http_status_entity->isNew(),
    ];

    /* You will need additional form elements for your custom properties. */

    return $form;
  }

  /**
   * @param array $form
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *
   * @return int|void
   * @throws \Drupal\Core\Entity\EntityMalformedException
   * @throws \Drupal\Core\Entity\EntityStorageException
   */
  public function save(array $form, FormStateInterface $form_state) {
    $http_status_entity = $this->entity;
    $status = $http_status_entity->save();
    $label = $http_status_entity->label();

    switch ($status) {
      case SAVED_NEW:
        $this->messenger()->addMessage($this->t('Created the %label HTTP Status entities.', ['%label' => $label]));
        break;

      default:
        $this->messenger()->addMessage($this->t('Saved the %label HTTP Status entities.', ['%label' => $label]));
    }

    $form_state->setRedirectUrl($http_status_entity->toUrl('collection'));
  }

  /**
   *
   */
  public function getStatusCodes() {
    $response = new Response();
    $text = $response::$statusTexts;
    foreach ($text as $key => $value) {
      $text[$key] = $value . ' ' . $key;
    }

    return $text;
  }

}
