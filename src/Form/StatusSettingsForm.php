<?php

namespace Drupal\http_status_code\Form;

use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Form\ConfigFormBase;

/**
 * Class StatusSettingsForm.
 */
class StatusSettingsForm extends ConfigFormBase {


  /**
 * @var string Config settings */
  const SETTINGS = 'http_status_code.settings';

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'status_settings_form';
  }

  /**
   * {@inheritdoc}
   */
  protected function getEditableConfigNames() {
    return [
      static::SETTINGS,
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {

    $config = $this->config(static::SETTINGS);

    $form['automatic_410'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Automatic 410'),
      '#description' => $this->t('Create a Status Code 410 for deleted entities (This is not implemented yet'),
      '#weight' => '0',
      '#default_value' => $config->get('automatic_410'),
    ];
    return parent::buildForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function validateForm(array &$form, FormStateInterface $form_state) {
    parent::validateForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {

    $this->configFactory->getEditable(static::SETTINGS)
          // Set the submitted configuration setting.
      ->set('automatic_410', $form_state->getValue('automatic_410'))
      ->save();
    parent::submitForm($form, $form_state);
  }

}
